# vasviewer

http://azsky2.html.xdomain.jp/

VapourSynth ビューア。<br>
スクリプトファイルを読み込んで、フレームの画像を表示します。

新しい VapourSynth API (ver 4) を使用しているので、最新の VapourSynth 上で動作します。

## 動作環境

- Linux、macOS(要XQuartz) ほか
- X11
- VapourSynth R56 以降<br>
(VapourSynth Script API 4.1 以降)

## コンパイル/インストール

ninja、pkg-config コマンドが必要です。<br>
各開発用ファイルのパッケージも必要になります。ReadMe を参照してください。

~~~
$ ./configure
$ cd build
$ ninja
# ninja install
~~~

