/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_COMBOBOX_H
#define MLK_COMBOBOX_H

#define MLK_COMBOBOX(p)  ((mComboBox *)(p))
#define MLK_COMBOBOX_DEF mWidget wg; mComboBoxData cb;

typedef struct
{
	mCIManager manager;
	int item_height;
	uint32_t fstyle;
}mComboBoxData;

struct _mComboBox
{
	mWidget wg;
	mComboBoxData cb;
};

enum MCOMBOBOX_NOTIFY
{
	MCOMBOBOX_N_CHANGE_SEL
};


#ifdef __cplusplus
extern "C" {
#endif

mComboBox *mComboBoxNew(mWidget *parent,int size,uint32_t fstyle);
mComboBox *mComboBoxCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);

void mComboBoxDestroy(mWidget *p);
void mComboBoxHandle_calcHint(mWidget *p);
void mComboBoxHandle_draw(mWidget *p,mPixbuf *pixbuf);
int mComboBoxHandle_event(mWidget *wg,mEvent *ev);

int mComboBoxGetItemNum(mComboBox *p);
void mComboBoxSetItemHeight(mComboBox *p,int height);
void mComboBoxSetItemHeight_min(mComboBox *p,int height);
void mComboBoxSetAutoWidth(mComboBox *p);

mColumnItem *mComboBoxAddItem(mComboBox *p,const char *text,uint32_t flags,intptr_t param);
mColumnItem *mComboBoxAddItem_static(mComboBox *p,const char *text,intptr_t param);
mColumnItem *mComboBoxAddItem_copy(mComboBox *p,const char *text,intptr_t param);
void mComboBoxAddItem_ptr(mComboBox *p,mColumnItem *item);

void mComboBoxAddItems_sepnull(mComboBox *p,const char *text,intptr_t param);
void mComboBoxAddItems_sepnull_arrayInt(mComboBox *p,const char *text,const int *array);
void mComboBoxAddItems_sepchar(mComboBox *p,const char *text,char split,intptr_t param);
void mComboBoxAddItems_tr(mComboBox *p,int idtop,int num,intptr_t param);

void mComboBoxDeleteAllItem(mComboBox *p);
void mComboBoxDeleteItem(mComboBox *p,mColumnItem *item);
void mComboBoxDeleteItem_index(mComboBox *p,int index);
mColumnItem *mComboBoxDeleteItem_select(mComboBox *p);

mColumnItem *mComboBoxGetTopItem(mComboBox *p);
mColumnItem *mComboBoxGetSelectItem(mComboBox *p);
mColumnItem *mComboBoxGetItem_atIndex(mComboBox *p,int index);
mColumnItem *mComboBoxGetItem_fromParam(mComboBox *p,intptr_t param);
mColumnItem *mComboBoxGetItem_fromText(mComboBox *p,const char *text);

int mComboBoxGetItemIndex(mComboBox *p,mColumnItem *item);
void mComboBoxGetItemText(mComboBox *p,int index,mStr *str);
intptr_t mComboBoxGetItemParam(mComboBox *p,int index);
void mComboBoxSetItemText_static(mComboBox *p,mColumnItem *item,const char *text);
void mComboBoxSetItemText_copy(mComboBox *p,int index,const char *text);

int mComboBoxGetSelIndex(mComboBox *p);
void mComboBoxSetSelItem(mComboBox *p,mColumnItem *item);
void mComboBoxSetSelItem_atIndex(mComboBox *p,int index);
mlkbool mComboBoxSetSelItem_fromParam(mComboBox *p,intptr_t param);
void mComboBoxSetSelItem_fromParam_notfound(mComboBox *p,intptr_t param,int notfound_index);
mlkbool mComboBoxSetSelItem_fromText(mComboBox *p,const char *text);

#ifdef __cplusplus
}
#endif

#endif
