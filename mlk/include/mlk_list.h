/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_LIST_H
#define MLK_LIST_H

#define MLK_LIST_FOR(list,item,itemtype)  for(item = (itemtype *)(list).top; item; item = (itemtype *)(((mListItem *)item)->next))
#define MLK_LIST_FOR_REV(list,item,itemtype) for(item = (itemtype *)(list).bottom; item; item = (itemtype *)(((mListItem *)item)->prev))

#ifdef __cplusplus
extern "C" {
#endif

mListItem *mListItemNew(int size);

void mListInit(mList *list);
mListItem *mListAppendNew(mList *list,int size);
mListItem *mListInsertNew(mList *list,mListItem *pos,int size);

void mListAppendItem(mList *list,mListItem *item);
void mListInsertItem(mList *list,mListItem *pos,mListItem *item);
void mListRemoveItem(mList *list,mListItem *item);

mlkbool mListDup(mList *dst,mList *src,int itemsize);

void mListDeleteAll(mList *list);
void mListDelete(mList *list,mListItem *item);
void mListDelete_no_handler(mList *list,mListItem *item);
mlkbool mListDelete_index(mList *list,int index);
void mListDelete_tops(mList *list,int num);
void mListDelete_bottoms(mList *list,int num);

mlkbool mListMove(mList *list,mListItem *src,mListItem *dst);
mlkbool mListMoveToTop(mList *list,mListItem *item);
mlkbool mListMoveUpDown(mList *list,mListItem *item,mlkbool up);
void mListSwapItem(mList *list,mListItem *item1,mListItem *item2);
void mListSort(mList *list,int (*comp)(mListItem *,mListItem *,void *),void *param);

mListItem *mListGetItemAtIndex(mList *list,int index);

int mListItemGetDir(mListItem *item1,mListItem *item2);
int mListItemGetIndex(mListItem *item);

void mListLinkAppend(mList *list,mListItem *item);
void mListLinkInsert(mList *list,mListItem *item,mListItem *pos);
void mListLinkRemove(mList *list,mListItem *item);

/* mListCache */

mListCacheItem *mListCache_appendNew(mList *list,int size);
void mListCache_refItem(mList *list,mListCacheItem *item);
void mListCache_releaseItem(mList *list,mListCacheItem *item);
void mListCache_deleteUnused(mList *list,int maxnum);
void mListCache_deleteUnused_allnum(mList *list,int num);

#ifdef __cplusplus
}
#endif

#endif
