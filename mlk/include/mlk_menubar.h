/*$
mlk
Copyright (c) 2020-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#ifndef MLK_MENUBAR_H
#define MLK_MENUBAR_H

#define MLK_MENUBAR(p)  ((mMenuBar *)(p))
#define MLK_MENUBAR_DEF mWidget wg; mMenuBarData menubar;

typedef struct
{
	mMenu *menu;
	mMenuItem *item_sel;
	uint32_t fstyle;
}mMenuBarData;

struct _mMenuBar
{
	mWidget wg;
	mMenuBarData menubar;
};

enum MMENUBAR_STYLE
{
	MMENUBAR_S_BORDER_BOTTOM = 1<<0
};

enum MMENUBAR_ARRAY16
{
	MMENUBAR_ARRAY16_END = 0xffff,
	MMENUBAR_ARRAY16_SEP = 0xfffe,
	MMENUBAR_ARRAY16_SUB_START = 0xfffd,
	MMENUBAR_ARRAY16_SUB_END = 0xfffc,
	MMENUBAR_ARRAY16_CHECK = 0x8000,
	MMENUBAR_ARRAY16_RADIO = 0x4000
};


#ifdef __cplusplus
extern "C" {
#endif

mMenuBar *mMenuBarNew(mWidget *parent,int size,uint32_t fstyle);
mMenuBar *mMenuBarCreate(mWidget *parent,int id,uint32_t flayout,uint32_t margin_pack,uint32_t fstyle);
void mMenuBarDestroy(mWidget *p);

void mMenuBarHandle_calcHint(mWidget *p);
int mMenuBarHandle_event(mWidget *p,mEvent *ev);
void mMenuBarHandle_draw(mWidget *p,mPixbuf *pixbuf);

mMenu *mMenuBarGetMenu(mMenuBar *p);
void mMenuBarSetMenu(mMenuBar *p,mMenu *menu);
void mMenuBarSetSubmenu(mMenuBar *p,int id,mMenu *submenu);

void mMenuBarCreateMenuTrArray16(mMenuBar *p,const uint16_t *buf);
void mMenuBarCreateMenuTrArray16_radio(mMenuBar *p,const uint16_t *buf);

#ifdef __cplusplus
}
#endif

#endif
