/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

#define APPNAME  "vasviewer"
#define RECENTFILE_NUM  6

typedef struct _MainWindow MainWindow;
typedef struct _ViewPage ViewPage;
typedef struct _VSData VSData;

typedef struct
{
	MainWindow *mainwin;
	mWidget *view;
	ViewPage *viewpage;

	VSData *vs;

	mStr str_curfile,	//現在のファイル
		str_opendir,	//最後に開いたディレクトリ
		str_savedir,	//画像保存ディレクトリ
		str_recentfile[RECENTFILE_NUM],	//ファイル履歴
		str_moveframe;	//フレーム移動テキスト

	int cur_frame;	//現在のフレーム位置
}AppData;

extern AppData g_app;

