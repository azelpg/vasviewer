/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * コントロール部分
 *****************************************/

#include <stdlib.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_event.h"
#include "mlk_label.h"
#include "mlk_lineedit.h"
#include "mlk_imgbutton.h"
#include "mlk_iconbar.h"
#include "mlk_imagelist.h"
#include "mlk_sysdlg.h"
#include "mlk_str.h"

#include "app.h"
#include "vs.h"
#include "mainwin.h"
#include "controls.h"
#include "timebar.h"
#include "trid.h"


//-----------------

struct _Controls
{
	MLK_CONTAINER_DEF

	TimeBar *timebar;
	mLineEdit *le_move,
		*le_curpos;
	
	mLabel *lb_status;

	mStr str_status;
};

enum
{
	WID_TIMEBAR = 100,
	WID_EDIT_MOVE,
	WID_BTT_MOVE,
	WID_BTT_HELP
};

static int _event_handle(mWidget *wg,mEvent *ev);

//-----------------

//移動ボタン (13x13 1bit)
static const unsigned char g_img_btt_move[]={
0x00,0x00,0x00,0x0e,0x00,0x0e,0x00,0x0e,0x10,0x0e,0x18,0x0e,0x1c,0x0e,0xfe,0x0f,
0xff,0x0f,0xfe,0x0f,0x1c,0x00,0x18,0x00,0x10,0x00 };

//ヘルプボタン (13x13, 1bit)
static const unsigned char g_img_btt_help[]={
0xf8,0x01,0xfc,0x03,0x0c,0x03,0x0c,0x03,0x0c,0x03,0x00,0x03,0xc0,0x01,0xe0,0x00,
0x60,0x00,0x60,0x00,0x00,0x00,0x60,0x00,0x60,0x00 };

//ツールバー (16x16 1bit)
#define _TOOLBAR_BTT_NUM 7

static const unsigned char g_img_toolbar[]={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x07,0x00,0x00,
0xfe,0x1f,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0x07,0xfe,0x00,0xfe,0x1f,
0x38,0x10,0x00,0x04,0x20,0x00,0x08,0x1c,0x03,0x06,0xfe,0x01,0x06,0x18,0x38,0x18,
0x00,0x06,0x60,0x00,0x18,0x1c,0x03,0x06,0x86,0x7f,0x06,0x18,0x38,0x1c,0x00,0x07,
0xe0,0x00,0x38,0x1c,0x03,0xfe,0xfe,0x7f,0x06,0x19,0x38,0x1e,0x80,0x07,0xe0,0x01,
0x78,0x1c,0x03,0xfe,0x06,0x60,0x86,0x01,0x38,0x1f,0xc0,0x07,0xe0,0x03,0xf8,0x1c,
0x03,0xc6,0x06,0x60,0xc6,0x7f,0xb8,0x1f,0xe0,0x07,0xe0,0x07,0xf8,0x1d,0x03,0xc6,
0x06,0x60,0xe6,0x7f,0xb8,0x1f,0xe0,0x07,0xe0,0x07,0xf8,0x1d,0x03,0xc6,0x06,0x60,
0xc6,0x7f,0x38,0x1f,0xc0,0x07,0xe0,0x03,0xf8,0x1c,0x03,0xc6,0x06,0x60,0x86,0x01,
0x38,0x1e,0x80,0x07,0xe0,0x01,0x78,0x1c,0xff,0xc7,0x06,0x60,0x06,0x19,0x38,0x1c,
0x00,0x07,0xe0,0x00,0x38,0x1c,0xff,0xc7,0x06,0x60,0x06,0x18,0x38,0x18,0x00,0x06,
0x60,0x00,0x18,0x1c,0x60,0xc0,0xfe,0x7f,0xfe,0x1f,0x38,0x10,0x00,0x04,0x20,0x00,
0x08,0x1c,0x60,0xc0,0xfe,0x7f,0xfe,0x1f,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0xe0,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xe0,0xff };

//-----------------



/* イメージボタン作成 */

static void _create_imgbtt(mWidget *parent,int id,const uint8_t *buf,int size)
{
	mImgButton *p;

	p = mImgButtonCreate(parent, id, MLF_MIDDLE, 0, 0);

	mImgButton_setBitImage(p, MIMGBUTTON_TYPE_1BIT_TP_TEXT, buf, size, size);
}

/* アイコンバー作成 */

static void _create_iconbar(mWidget *parent)
{
	mIconBar *ib;
	int i;
	uint16_t id[] = {
		TRID_MENU_FILE_OPEN, TRID_MENU_FILE_RELOAD, TRID_MENU_EDIT_TOP, TRID_MENU_EDIT_PREV_FRAME,
		TRID_MENU_EDIT_NEXT_FRAME, TRID_MENU_EDIT_BOTTOM, TRID_MENU_EDIT_COPY_FRAME_POS
	};

	ib = mIconBarCreate(parent, 0, MLF_MIDDLE, 0,
		MICONBAR_S_TOOLTIP | MICONBAR_S_BUTTON_FRAME | MICONBAR_S_DESTROY_IMAGELIST);

	mIconBarSetImageList(ib,
		mImageListCreate_1bit_textcol(g_img_toolbar, 16 * _TOOLBAR_BTT_NUM, 16, 16));

	mIconBarSetTooltipTrGroup(ib, TRGROUP_ID_TOOLTIP);

	for(i = 0; i < _TOOLBAR_BTT_NUM; i++)
		mIconBarAdd(ib, id[i], i, i, 0);
}

/* 破棄 */

static void _destroy_handle(mWidget *wg)
{
	Controls *p = (Controls *)wg;

	mStrFree(&p->str_status);
}

/** 作成 */

Controls *controls_create(mWidget *parent)
{
	Controls *p;
	mWidget *ct;

	p = (Controls *)mContainerNew(parent, sizeof(Controls));
	if(!p) return NULL;

	p->wg.flayout = MLF_EXPAND_W;
	p->wg.event = _event_handle;
	p->wg.destroy = _destroy_handle;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;

	mContainerSetType_vert(MLK_CONTAINER(p), 5);
	mContainerSetPadding_same(MLK_CONTAINER(p), 4);

	//----- バー部分

	ct = mContainerCreateHorz(MLK_WIDGET(p), 4, MLF_EXPAND_W, 0);

	p->timebar = timebar_create(ct, WID_TIMEBAR);

	//移動エディット

	p->le_move = mLineEditCreate(ct, WID_EDIT_MOVE, MLF_MIDDLE, MLK_MAKE32_4(2,0,0,0),
		MLINEEDIT_S_NOTIFY_ENTER);

	mLineEditSetWidth_textlen(p->le_move, 9);
	mLineEditSetText(p->le_move, g_app.str_moveframe.buf);

	//移動ボタン

	_create_imgbtt(ct, WID_BTT_MOVE, g_img_btt_move, 13);

	//ヘルプボタン

	_create_imgbtt(ct, WID_BTT_HELP, g_img_btt_help, 13);

	//----- 真ん中

	ct = mContainerCreateHorz(MLK_WIDGET(p), 4, MLF_EXPAND_W, 0);

	_create_iconbar(ct);

	p->le_curpos = mLineEditCreate(ct, 0, MLF_EXPAND_W | MLF_MIDDLE, 0,
		MLINEEDIT_S_READ_ONLY);

	//----- ステータス部分
	
	p->lb_status = mLabelCreate(MLK_WIDGET(p), MLF_EXPAND_W, 0, MLABEL_S_BORDER, NULL);

	//VapourSynth 初期化失敗時、エラーメッセージをセット

	if(vs_get_errmes(g_app.vs, &p->str_status))
		mLabelSetText(p->lb_status, p->str_status.buf);
	
	return p;
}

/** アプリ終了前 */

void controls_save(Controls *p)
{
	mLineEditGetTextStr(p->le_move, &g_app.str_moveframe);
}

/** スクリプトファイルの更新時
 *
 * 開いた時 or 再読込時 */

void controls_change_file(Controls *p)
{
	//バー

	timebar_setinfo(p->timebar, vs_get_frames(g_app.vs), g_app.cur_frame);

	//ステータス
	// :エラー時、複数行の場合あり

	if(!vs_get_errmes(g_app.vs, &p->str_status))
		vs_get_video_info(g_app.vs, &p->str_status);

	mLabelSetText(p->lb_status, p->str_status.buf);

	mWidgetReLayout(MLK_WIDGET(p->wg.toplevel));
}

/** フレームが取得された時
 *
 * MainWindow から呼ばれる */

void controls_on_getframe(Controls *p)
{
	VSTime t;
	mStr str = MSTR_INIT;

	//バー位置

	timebar_setpos(p->timebar, g_app.cur_frame);

	//フレーム位置

	if(!vs_get_errmes(g_app.vs, &str))
	{
		vs_get_frame_time(g_app.vs, g_app.cur_frame, &t);

		mStrSetFormat(&str, "%d / %d:%02d:%02d.%03d",
			g_app.cur_frame, t.h, t.m, t.s, t.ms);
	}

	mLineEditSetText(p->le_curpos, str.buf);

	mStrFree(&str);
}

/* テキストからフレーム移動 */

static void _move_frame(Controls *p)
{
	char *pc,*end;
	int sig,ftime,n;
	double h,m,s,ms,frame,d;

	//----- テキスト解析

	mLineEditGetTextStr(p->le_move, &g_app.str_moveframe);

	pc = g_app.str_moveframe.buf;

	sig = 0;
	ftime = 0;
	h = m = s = ms = frame = 0;

	//符号

	if(*pc == '+')
		sig = 1, pc++;
	else if(*pc == '-')
		sig = -1, pc++;

	//

	while(*pc)
	{
		d = strtod(pc, &end);

		//単位無しでフレーム位置
		
		if(!(*end))
		{
			frame = d;
			ftime = 0;
			break;
		}

		//単位

		ftime = 0;

		switch(*end)
		{
			case 'h':
			case 'H':
				h = d;
				ftime = 1;
				break;
			case 'm':
			case 'M':
				if(end[1] == 's' || end[1] == 'S')
					ms = d, end++;
				else
					m = d;
				
				ftime = 1;
				break;
			case 's':
			case 'S':
				s = d;
				ftime = 1;
				break;
		}

		if(!ftime) break;

		pc = end + 1;
	}

	//------- 移動

	if(ftime)
	{
		frame = h * 3600 + m * 60 + s + ms / 1000; //sec
		frame *= vs_get_fps(g_app.vs);
	}

	n = (int)(frame + 0.5);

	if(sig == 0)
		MainWindow_moveFrame(g_app.mainwin, n);
	else
	{
		if(sig == -1) n = -n;
		
		MainWindow_moveFrame_add(g_app.mainwin, n);
	}
}

/* イベントハンドラ */

int _event_handle(mWidget *wg,mEvent *ev)
{
	if(ev->type == MEVENT_NOTIFY)
	{
		switch(ev->notify.id)
		{
			//バー: 位置変更時
			case WID_TIMEBAR:
				MainWindow_moveFrame(g_app.mainwin, ev->notify.param1);
				break;
			//フレーム移動エディット
			case WID_EDIT_MOVE:
				if(ev->notify.notify_type == MLINEEDIT_N_ENTER)
					_move_frame((Controls *)wg);
				break;
			//フレーム移動ボタン
			case WID_BTT_MOVE:
				_move_frame((Controls *)wg);
				break;
			//フレーム移動ヘルプ
			case WID_BTT_HELP:
				mMessageBoxOK(wg->toplevel, MLK_TR2(TRGROUP_ID_HELP, TRID_HELP_MOVE_FRAME));
				break;
		}
	}
	else if(ev->type == MEVENT_COMMAND)
	{
		//アイコンバー

		mWidgetEventAdd_command(MLK_WIDGET(g_app.mainwin),
			ev->cmd.id, 0, 0, NULL);
	}

	return 1;
}


