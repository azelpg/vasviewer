/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/**********************************
 * main
 **********************************/

#include <stdio.h>
#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_window.h"
#include "mlk_str.h"
#include "mlk_iniread.h"
#include "mlk_iniwrite.h"

#include "app.h"
#include "mainwin.h"
#include "vs.h"

#include "deftrans.h"


//--------------

typedef struct
{
	mToplevelSaveState state_main;
}_initconfig;

AppData g_app;

#define CONFIG_FILENAME "vasviewer.conf"

#define _HELP_TEXT "[usage] exe <file>\n\n" \
 "--help-mlk : display mlk options"

//--------------



//===========================
// 設定ファイル
//===========================


/* ウィンドウ状態読み込み */

static void _read_config_winstate(mIniRead *ini,const char *key,mToplevelSaveState *state)
{
	int32_t n[7];

	//[!] state はゼロクリアされているので、初期値は設定しなくてよい

	if(mIniRead_getNumbers(ini, key, n, 7, 4, FALSE) == 7)
	{
		state->x = n[0];
		state->y = n[1];
		state->w = n[2];
		state->h = n[3];
		state->norm_x = n[4];
		state->norm_y = n[5];
		state->flags = n[6];
	}
}

/* ウィンドウ状態書き込み */

static void _save_config_winstate(FILE *fp,const char *key,mToplevel *win)
{
	mToplevelSaveState st;
	int32_t n[7];

	mToplevelGetSaveState(win, &st);

	n[0] = st.x;
	n[1] = st.y;
	n[2] = st.w;
	n[3] = st.h;
	n[4] = st.norm_x;
	n[5] = st.norm_y;
	n[6] = st.flags;

	mIniWrite_putNumbers(fp, key, n, 7, 4, FALSE);
}

/* 設定ファイル読み込み */

static void _read_config(_initconfig *dst)
{
	mIniRead *ini;
	AppData *p = &g_app;

	mIniRead_loadFile_join(&ini, mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!ini) return;

	//------- main

	mIniRead_setGroup(ini, "main");

	//メインウィンドウ

	_read_config_winstate(ini, "mainwin", &dst->state_main);

	//

	mIniRead_getTextStr(ini, "opendir", &p->str_opendir, NULL);
	mIniRead_getTextStr(ini, "savedir", &p->str_savedir, NULL);
	mIniRead_getTextStr(ini, "moveframe", &p->str_moveframe, NULL);

	//-------- ファイル履歴

	mIniRead_setGroup(ini, "recentfile");
	mIniRead_getTextStrArray(ini, 0, p->str_recentfile, RECENTFILE_NUM);

	//---- mlk 情報

	mGuiReadIni_system(ini);

	//

	mIniRead_end(ini);
}

/* 設定ファイル書き込み */

static void _save_config(void)
{
	FILE *fp;
	AppData *p = &g_app;

	fp = mIniWrite_openFile_join(mGuiGetPath_config_text(), CONFIG_FILENAME);
	if(!fp) return;

	//-------- メイン

	mIniWrite_putGroup(fp, "main");

	//メインウィンドウ

	_save_config_winstate(fp, "mainwin", MLK_TOPLEVEL(p->mainwin)); 

	//

	mIniWrite_putStr(fp, "opendir", &p->str_opendir);
	mIniWrite_putStr(fp, "savedir", &p->str_savedir);
	mIniWrite_putStr(fp, "moveframe", &p->str_moveframe);

	//------ ファイル履歴

	mIniWrite_putGroup(fp, "recentfile");
	mIniWrite_putStrArray(fp, 0, p->str_recentfile, RECENTFILE_NUM);

	//------ mlk

	mGuiWriteIni_system(fp);

	fclose(fp);
}


//===========================
// 初期化
//===========================


/* 初期化 */

static int _init(int argc,char **argv,int toparg)
{
	_initconfig conf;

	mMemset0(&g_app, sizeof(AppData));
	mMemset0(&conf, sizeof(_initconfig));

	//設定ファイルディレクトリ作成

	mGuiCreateConfigDir(NULL);

	//設定ファイル読み込み

	_read_config(&conf);

	//VapourSynth 初期化

	g_app.vs = vs_init();
	if(!g_app.vs) return 1;

	//メインウィンドウ作成・表示

	MainWindowNew(&conf.state_main);

	//ファイルを開く

	if(toparg < argc)
	{
		//引数のファイル

		mStr str = MSTR_INIT;

		mStrSetText_locale(&str, argv[toparg], -1);
		
		MainWindow_loadFile(g_app.mainwin, str.buf);

		mStrFree(&str);
	}

	return 0;
}


//===========================
// メイン
//===========================


/* AppData 解放 */

static void _free_app(AppData *p)
{
	vs_free(p->vs);

	mStrFree(&p->str_curfile);
	mStrFree(&p->str_opendir);
	mStrFree(&p->str_savedir);
	mStrFree(&p->str_moveframe);
	
	mStrArrayFree(p->str_recentfile, RECENTFILE_NUM);
}

/* 終了処理 */

static void _finish(void)
{
	_save_config();

	_free_app(&g_app);
}

/* 初期化メイン */

static int _init_main(int argc,char **argv)
{
	int top,i;

	if(mGuiInit(argc, argv, &top)) return 1;

	//"--help"

	for(i = top; i < argc; i++)
	{
		if(strcmp(argv[i], "--help") == 0)
		{
			puts(_HELP_TEXT);
			mGuiEnd();
			return 1;
		}
	}

	//

	mGuiSetWMClass(APPNAME, APPNAME);

	//パスセット

	mGuiSetPath_data_exe("../share/vasviewer");
	mGuiSetPath_config_home(".config/vasviewer");

	//翻訳データ

	mGuiLoadTranslation(g_deftransdat, NULL, NULL);

	//バックエンド初期化

	if(mGuiInitBackend()) return 1;

	//初期化

	if(_init(argc, argv, top))
	{
		mError("failed initialize\n");
		return 1;
	}

	return 0;
}

/** メイン */

int main(int argc,char **argv)
{
	//初期化
	
	if(_init_main(argc, argv)) return 1;

	//実行

	mGuiRun();

	//終了

	_finish();

	mGuiEnd();

	return 0;
}

