/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * MainWindow
 *****************************************/

#include <string.h>

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_window.h"
#include "mlk_event.h"
#include "mlk_menubar.h"
#include "mlk_menu.h"
#include "mlk_accelerator.h"
#include "mlk_sysdlg.h"
#include "mlk_clipboard.h"
#include "mlk_str.h"
#include "mlk_key.h"
#include "mlk_string.h"
#include "mlk_saveimage.h"
#include "mlk_imagebuf.h"

#include "app.h"
#include "vs.h"
#include "mainwin.h"
#include "view.h"
#include "controls.h"
#include "trid.h"

#include "def_menudata.h"


//----------------------

#define _VERSION_TEXT  APPNAME " ver 1.0.2\n\nCopyright (c) 2021-2022 Azel"

static int _event_handle(mWidget *wg,mEvent *ev);

//ショートカットキー
static const uint32_t g_sckey_def[] = {
	TRID_MENU_FILE_OPEN, MLK_ACCELKEY_CTRL | 'O',
	TRID_MENU_FILE_RELOAD, MKEY_F5,
	TRID_MENU_FILE_SAVE_IMAGE, MLK_ACCELKEY_CTRL | 'S',
	0
};

//----------------------


//=========================
// 作成 - sub
//=========================


/* メニュー作成 */

static void _create_menu(MainWindow *p)
{
	mMenuBar *bar;
	mMenu *menu_top;
	const uint32_t *ps;

	bar = mMenuBarNew(MLK_WIDGET(p), 0, MMENUBAR_S_BORDER_BOTTOM);

	mToplevelAttachMenuBar(MLK_TOPLEVEL(p), bar);

	//データから項目をセット

	MLK_TRGROUP(TRGROUP_ID_MENU);

	mMenuBarCreateMenuTrArray16(bar, g_menudata);

	menu_top = mMenuBarGetMenu(bar);

	//ショートカットキーセット

	for(ps = g_sckey_def; *ps; ps += 2)
		mMenuSetItemShortcutKey(menu_top, ps[0], ps[1]);

	//ファイル履歴サブメニュー

	p->menu_recentfile = mMenuNew();

	mMenuSetItemSubmenu(menu_top, TRID_MENU_FILE_RECENTFILE, p->menu_recentfile);

	MainWindow_setRecentFileMenu(p);
}

/* アクセラレータ作成 */

static void _create_accel(MainWindow *p)
{
	mAccelerator *accel;
	const uint32_t *ps;

	accel = mAcceleratorNew();

	mToplevelAttachAccelerator(MLK_TOPLEVEL(p), accel);

	mAcceleratorSetDefaultWidget(accel, MLK_WIDGET(p));

	for(ps = g_sckey_def; *ps; ps += 2)
		mAcceleratorAdd(accel, ps[0], ps[1], NULL);
}


//=========================
// main
//=========================


/* 破棄ハンドラ */

static void _destroy_handle(mWidget *p)
{
	mToplevelDestroyAccelerator(MLK_TOPLEVEL(p));
}

/** メインウィンドウ作成・表示 */

void MainWindowNew(mToplevelSaveState *state)
{
	MainWindow *p;

	//ウィンドウ
	
	p = (MainWindow *)mToplevelNew(NULL, sizeof(MainWindow),
			MTOPLEVEL_S_NORMAL | MTOPLEVEL_S_TAB_MOVE | MTOPLEVEL_S_NO_INPUT_METHOD);
	if(!p) return;
	
	g_app.mainwin = p;

	p->wg.destroy = _destroy_handle;
	p->wg.event = _event_handle;

	p->wg.fstate |= MWIDGET_STATE_ENABLE_DROP;

	//フレームを取得した時通知

	vs_set_notify_widget(g_app.vs, MLK_WIDGET(p));

	//タイトル

	mToplevelSetTitle(MLK_TOPLEVEL(p), APPNAME);

	//作成

	_create_menu(p);
	_create_accel(p);

	view_create(MLK_WIDGET(p));

	p->ctrl = controls_create(MLK_WIDGET(p));

	//状態復元

	if(!state->w || !state->h)
		state->w = state->h = 500;

	mToplevelSetSaveState(MLK_TOPLEVEL(p), state);

	//表示

	mWidgetShow(MLK_WIDGET(p), 1);
}

/** ファイル履歴のメニューをセット */

void MainWindow_setRecentFileMenu(MainWindow *p)
{
	mMenuDeleteAll(p->menu_recentfile);

	mMenuAppendStrArray(p->menu_recentfile, g_app.str_recentfile,
		MAINWIN_CMDID_RECENTFILE, RECENTFILE_NUM);
}

/** ウィンドウタイトルセット */

void MainWindow_setTitle(MainWindow *p)
{
	mStr str = MSTR_INIT;

	if(mStrIsnotEmpty(&g_app.str_curfile))
	{
		mStrPathGetBasename(&str, g_app.str_curfile.buf);
		mStrAppendText(&str, " - ");
	}

	mStrAppendText(&str, APPNAME);

	mToplevelSetTitle(MLK_TOPLEVEL(p), str.buf);

	mStrFree(&str);
}

/** フレーム取得待ち状態かどうか */

int MainWindow_waitFrame(MainWindow *p)
{
	if(vs_is_waitframe(g_app.vs))
	{
		mMessageBoxOK(MLK_WINDOW(p), MLK_TR2(TRGROUP_ID_MESSAGE, TRID_MES_WAIT_FRAME));
		return 1;
	}

	return 0;
}

/** ファイル読み込み */

void MainWindow_loadFile(MainWindow *p,const char *filename)
{
	if(MainWindow_waitFrame(p)) return;

	g_app.cur_frame = 0;

	//読み込み (失敗時も続ける)

	vs_loadfile(g_app.vs, filename);

	//--------

	//最後に開いたディレクトリ

	mStrPathGetDir(&g_app.str_opendir, filename);

	//現在のファイル名

	mStrSetText(&g_app.str_curfile, filename);

	//タイトル

	MainWindow_setTitle(p);

	//ファイル履歴
	//[!] str_curfile をセットした後に

	mStrArrayAddRecent(g_app.str_recentfile, RECENTFILE_NUM, filename);

	//履歴メニュー

	MainWindow_setRecentFileMenu(p);

	//-------

	controls_change_file(p->ctrl);

	//ビュー:スクロール

	view_set_scroll_info(TRUE);

	//先頭フレーム

	vs_getframe(g_app.vs, 0);
}

/** ファイル開くダイアログ */

void MainWindow_openDialog(MainWindow *p)
{
	mStr str = MSTR_INIT;

	if(MainWindow_waitFrame(p)) return;

	if(mSysDlg_openFile(MLK_WINDOW(p), "All Files\t*", 0,
		g_app.str_opendir.buf, 0, &str))
	{
		MainWindow_loadFile(p, str.buf);
	
		mStrFree(&str);
	}
}

/* 再読込
 *
 * フレーム位置はそのまま */

static void _file_reload(MainWindow *p)
{
	if(MainWindow_waitFrame(p)
		|| mStrIsEmpty(&g_app.str_curfile))
		return;

	if(vs_loadfile(g_app.vs, g_app.str_curfile.buf))
	{
		//現在位置を元に、フレーム位置調整

		g_app.cur_frame = vs_adjust_frame_pos(g_app.vs, g_app.cur_frame);

		//

		vs_getframe(g_app.vs, g_app.cur_frame);
	}

	controls_change_file(p->ctrl);
}

/** フレーム位置移動 (絶対位置)
 *
 * ※フレーム未取得の状態でも、メッセージは表示しない。 */

void MainWindow_moveFrame(MainWindow *p,int pos)
{
	if(vs_is_waitframe(g_app.vs)) return;

	pos = vs_adjust_frame_pos(g_app.vs, pos);

	if(pos != g_app.cur_frame)
	{
		g_app.cur_frame = pos;
	
		vs_getframe(g_app.vs, pos);
	}
}

/** フレーム移動 (相対) */

void MainWindow_moveFrame_add(MainWindow *p,int n)
{
	MainWindow_moveFrame(p, g_app.cur_frame + n);
}

/* フレーム位置をコピー */

static void _copy_framepos(MainWindow *p)
{
	char m[16];

	mIntToStr(m, g_app.cur_frame);

	mClipboardSetText(m, -1);
}

/* フレーム位置貼り付け */

static void _paste_framepos(MainWindow *p)
{
	mStr str = MSTR_INIT;

	if(mClipboardGetText(&str) != MCLIPBOARD_RET_OK)
		mMessageBoxErrTr(MLK_WINDOW(p), TRGROUP_ID_MESSAGE, TRID_MES_PASTE_ERR);
	else
		MainWindow_moveFrame(p, mStrToInt(&str));

	mStrFree(&str);
}


//========================
// 画像保存
//========================


/* Y1行イメージセット */

static mlkerr _save_setrow(mSaveImage *p,int y,uint8_t *buf,int line_bytes)
{
	memcpy(buf, *((uint8_t **)p->param1 + y), line_bytes);

	return MLKERR_OK;
}

/* 画像ファイル書き込み */

static mlkerr _saveimage_write(mImageBuf2 *img,const char *filename)
{
	mSaveImage si;

	mSaveImage_init(&si);

	si.open.type = MSAVEIMAGE_OPEN_FILENAME;
	si.open.filename = filename;
	si.width = img->width;
	si.height = img->height;
	si.coltype = MSAVEIMAGE_COLTYPE_RGB;
	si.bits_per_sample = 8;
	si.samples_per_pixel = 3;
	si.setrow = _save_setrow;
	si.param1 = img->ppbuf;

	//保存

	return mSaveImagePNG(&si, NULL);
}

/* 画像保存 */

static void _saveimage(MainWindow *p)
{
	mStr str = MSTR_INIT;
	mImageBuf2 *img;

	if(MainWindow_waitFrame(p)) return;

	img = vs_get_frame_image(g_app.vs);
	if(!img) return;

	//

	if(mSysDlg_saveFile(MLK_WINDOW(p), "PNG (*.png)\tpng", 0,
		g_app.str_savedir.buf, 0, &str, NULL))
	{
		mStrPathGetDir(&g_app.str_savedir, str.buf);

		if(_saveimage_write(img, str.buf))
			mMessageBoxErrTr(MLK_WINDOW(p), TRGROUP_ID_MESSAGE, TRID_MES_SAVEIMAGE_ERR);
	
		mStrFree(&str);
	}
}


//========================
// イベント
//========================


/* キー処理
 *
 * ビュー・タイムバーにフォーカスがあって、キーが押された時。
 * エディットでも使われるキーの場合、エディット操作を優先する。 */

static void _event_key(MainWindow *p,int key,int state)
{
	int fctrl;

	fctrl = ((state & MLK_STATE_MASK_MODS) == MLK_STATE_CTRL);

	switch(key)
	{
		case MKEY_LEFT:
			MainWindow_moveFrame_add(p, -1);
			break;
		case MKEY_RIGHT:
			MainWindow_moveFrame_add(p, 1);
			break;
		case MKEY_HOME:
			MainWindow_moveFrame(p, 0);
			break;
		case MKEY_END:
			MainWindow_moveFrame(p, vs_get_frames(g_app.vs) - 1);
			break;
		case 'C':
		case 'c':
			if(fctrl)
				_copy_framepos(p);
			break;
		case 'V':
		case 'v':
			if(fctrl)
				_paste_framepos(p);
			break;
	}
}

/* フレーム取得
 *
 * VapourSynth からフレームが取得された後に来る */

static void _event_getframe(MainWindow *p)
{
	controls_on_getframe(p->ctrl);

	view_update();
}

/* 終了 */

static void _app_quit(MainWindow *p)
{
	controls_save(p->ctrl);

	mGuiQuit();
}

/* COMMAND イベント */

static void _event_command(mWidget *wg,mEvent *ev)
{
	MainWindow *p = MAINWINDOW(wg);
	int id = ev->cmd.id;

	//ファイル履歴

	if(id >= MAINWIN_CMDID_RECENTFILE && id < MAINWIN_CMDID_RECENTFILE + RECENTFILE_NUM)
	{
		MainWindow_loadFile(p, g_app.str_recentfile[id - MAINWIN_CMDID_RECENTFILE].buf);
		return;
	}

	//

	switch(id)
	{
		//---- ファイル

		//開く
		case TRID_MENU_FILE_OPEN:
			MainWindow_openDialog(p);
			break;
		//再読込
		case TRID_MENU_FILE_RELOAD:
			_file_reload(p);
			break;
		//画像を保存
		case TRID_MENU_FILE_SAVE_IMAGE:
			_saveimage(p);
			break;
		//終了
		case TRID_MENU_FILE_EXIT:
			_app_quit(p);
			break;

		//---- 編集

		//1フレーム戻る
		case TRID_MENU_EDIT_PREV_FRAME:
			MainWindow_moveFrame_add(p, -1);
			break;
		//1フレーム進む
		case TRID_MENU_EDIT_NEXT_FRAME:
			MainWindow_moveFrame_add(p, 1);
			break;
		//先頭
		case TRID_MENU_EDIT_TOP:
			MainWindow_moveFrame(p, 0);
			break;
		//終端
		case TRID_MENU_EDIT_BOTTOM:
			MainWindow_moveFrame(p, vs_get_frames(g_app.vs) - 1);
			break;
		//フレーム位置をコピー
		case TRID_MENU_EDIT_COPY_FRAME_POS:
			_copy_framepos(p);
			break;
		//フレーム位置貼り付け
		case TRID_MENU_EDIT_PASTE_FRAME_POS:
			_paste_framepos(p);
			break;

		//---- ヘルプ

		//バージョン情報
		case TRID_MENU_HELP_ABOUT:
			mSysDlg_about(MLK_WINDOW(wg), _VERSION_TEXT);
			break;
	}
}

/* イベントハンドラ */

int _event_handle(mWidget *wg,mEvent *ev)
{
	switch(ev->type)
	{
		//フレーム取得
		case VS_EVENT_GETFRAME:
			_event_getframe(MAINWINDOW(wg));
			break;
		//通知
		case MEVENT_NOTIFY:
			if(ev->notify.id == -1)
				_event_key(MAINWINDOW(wg), ev->notify.param1, ev->notify.param2);
			break;

		//コマンド
		case MEVENT_COMMAND:
			_event_command(wg, ev);
			break;
		//ファイルドロップ
		case MEVENT_DROP_FILES:
			MainWindow_loadFile(MAINWINDOW(wg), *(ev->dropfiles.files));
			break;
	
		//閉じるボタン
		case MEVENT_CLOSE:
			_app_quit(MAINWINDOW(wg));
			break;
		
		default:
			return FALSE;
	}

	return TRUE;
}

