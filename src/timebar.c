/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * TimeBar
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_event.h"
#include "mlk_pixbuf.h"
#include "mlk_guicol.h"

#include "timebar.h"


/* 位置の変更時、通知される
 *
 * param1 = フレーム位置 */

//---------------

struct _TimeBar
{
	mWidget wg;

	int frames,
		pos;	//現在のフレーム位置
};

//---------------



/* イベント */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	TimeBar *p = (TimeBar *)wg;

	switch(ev->type)
	{
		case MEVENT_POINTER:
			if(ev->pt.act == MEVENT_POINTER_ACT_PRESS
				&& ev->pt.btt == MLK_BTT_LEFT)
			{
				mWidgetSetFocus(wg);

				//フレーム位置変更
				// :カーソルは、フレームが取得できた時に移動する

				if(p->frames)
				{
					mWidgetEventAdd_notify(wg, NULL, 0,
						(int)((double)ev->pt.x / (wg->w - 1) * (p->frames - 1) + 0.5), 0);
				}
			}
			break;

		case MEVENT_KEYDOWN:
			//メインウィンドウに通知
			if(!ev->key.is_grab_pointer)
			{
				mWidgetEventAdd_notify_id(wg, MLK_WIDGET(wg->toplevel), -1, 0,
					ev->key.key, ev->key.state);
			}
			break;
	}

	return 1;
}

/* 描画ハンドラ */

static void _draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	TimeBar *p = (TimeBar *)wg;
	int w,h,i;
	mPixCol col;

	w = wg->w;
	h = wg->h;

	col = MGUICOL_PIX(FRAME);

	//背景

	mWidgetDrawBkgnd(wg, NULL);

	//枠

	mPixbufBox(pixbuf, 0, 0, w, h, col);

	//線

	mPixbufLineH(pixbuf, 0, h / 2, w, col);

	for(i = 1; i <= 3; i++)
		mPixbufLineV(pixbuf, i * w / 4, 2, h - 4, col);

	//カーソル

	if(p->frames)
	{
		i = (int)((double)p->pos / (p->frames - 1) * (w - 1) + 0.5);

		mPixbufFillBox(pixbuf, i - 1, 0, 3, h, MGUICOL_PIX(TEXT));
	}
}

/** 作成 */

TimeBar *timebar_create(mWidget *parent,int id)
{
	TimeBar *p;

	p = (TimeBar *)mWidgetNew(parent, sizeof(TimeBar));
	if(!p) return NULL;

	p->wg.id = id;
	p->wg.draw = _draw_handle;
	p->wg.event = _event_handle;
	p->wg.flayout = MLF_EXPAND_W | MLF_MIDDLE;
	p->wg.fevent = MWIDGET_EVENT_POINTER | MWIDGET_EVENT_KEY;
	p->wg.fstate |= MWIDGET_STATE_TAKE_FOCUS | MWIDGET_STATE_ENABLE_KEY_REPEAT;
	p->wg.hintH = 17;

	return p;
}

/** 情報セット */

void timebar_setinfo(TimeBar *p,int frames,int pos)
{
	p->frames = frames;
	p->pos = pos;

	mWidgetRedraw(MLK_WIDGET(p));
}

/** 位置セット */

void timebar_setpos(TimeBar *p,int pos)
{
	if(pos != p->pos)
	{
		p->pos = pos;

		mWidgetRedraw(MLK_WIDGET(p));
	}
}

