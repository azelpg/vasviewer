/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/********************************
 * 翻訳文字列ID
 ********************************/

/* グループID */

enum TRGROUP_ID
{
	TRGROUP_ID_TOOLTIP = 1,
	TRGROUP_ID_MESSAGE = 100,
	TRGROUP_ID_HELP,
	
	TRGROUP_ID_MENU = 1000
};

/* メッセージ */

enum
{
	TRID_MES_WAIT_FRAME = 0,	//フレームがまだ取得されていない
	TRID_MES_PASTE_ERR,			//貼り付けエラー
	TRID_MES_SAVEIMAGE_ERR		//画像保存エラー
};

/* ヘルプ */

enum
{
	TRID_HELP_MOVE_FRAME = 0	//フレーム移動
};

/* メニュー */

enum
{
	//トップ
	TRID_MENU_TOP_FILE = 1,
	TRID_MENU_TOP_EDIT,
	TRID_MENU_TOP_HELP,

	//ファイル
	TRID_MENU_FILE_OPEN = 1000,
	TRID_MENU_FILE_RELOAD,
	TRID_MENU_FILE_SAVE_IMAGE,
	TRID_MENU_FILE_RECENTFILE,
	TRID_MENU_FILE_EXIT,

	//編集
	TRID_MENU_EDIT_PREV_FRAME = 1100,
	TRID_MENU_EDIT_NEXT_FRAME,
	TRID_MENU_EDIT_TOP,
	TRID_MENU_EDIT_BOTTOM,
	TRID_MENU_EDIT_COPY_FRAME_POS,
	TRID_MENU_EDIT_PASTE_FRAME_POS,

	//ヘルプ
	TRID_MENU_HELP_ABOUT = 1200
};

