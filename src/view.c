/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * ビュー部分
 *****************************************/

#include "mlk_gui.h"
#include "mlk_widget_def.h"
#include "mlk_widget.h"
#include "mlk_event.h"
#include "mlk_scrollbar.h"
#include "mlk_scrollview.h"
#include "mlk_pixbuf.h"
#include "mlk_guicol.h"
#include "mlk_imagebuf.h"

#include "app.h"
#include "vs.h"
#include "view.h"


//-----------------

struct _ViewPage
{
	MLK_SCROLLVIEWPAGE_DEF

	int scrx,scry,
		fdrag;
	mPoint ptlast;
};

//-----------------


/* ボタン押し時 */

static void _page_press(ViewPage *p,mEventPointer *ev)
{
	mSize size;

	vs_get_video_size(g_app.vs, &size);

	if(size.w > p->wg.w || size.h > p->wg.h)
	{
		p->fdrag = 1;
		p->ptlast.x = ev->x;
		p->ptlast.y = ev->y;
		
		mWidgetGrabPointer(MLK_WIDGET(p));
	}
}

/* ドラッグ移動 : スクロール */

static void _page_motion(ViewPage *p,int x,int y)
{
	View *pv = (View *)p->wg.parent;
	int dx,dy;
	mPoint pt,ptnew;

	dx = p->ptlast.x - x;
	dy = p->ptlast.y - y;

	if(dx == 0 && dy == 0) return;

	pt.x = p->scrx;
	pt.y = p->scry;

	mScrollBarSetPos(pv->sv.scrh, pt.x + dx);
	mScrollBarSetPos(pv->sv.scrv, pt.y + dy);

	ptnew.x = mScrollBarGetPos(pv->sv.scrh);
	ptnew.y = mScrollBarGetPos(pv->sv.scrv);

	if(pt.x != ptnew.x || pt.y != ptnew.y)
	{
		p->scrx = ptnew.x;
		p->scry = ptnew.y;

		mWidgetRedraw(MLK_WIDGET(p));
	}
	
	p->ptlast.x = x;
	p->ptlast.y = y;
}

/* グラブ解放 */

static void _page_ungrab(ViewPage *p)
{
	if(p->fdrag)
	{
		p->fdrag = 0;
		mWidgetUngrabPointer();
	}
}

/* イベント */

static int _page_event_handle(mWidget *wg,mEvent *ev)
{
	ViewPage *p = (ViewPage *)wg;

	switch(ev->type)
	{
		//ポインタ
		case MEVENT_POINTER:
			switch(ev->pt.act)
			{
				//移動
				case MEVENT_POINTER_ACT_MOTION:
					if(p->fdrag)
						_page_motion(p, ev->pt.x, ev->pt.y);
					break;
				//押し
				case MEVENT_POINTER_ACT_PRESS:
					if(!p->fdrag)
					{
						mWidgetSetFocus(wg);
					
						if(ev->pt.btt == MLK_BTT_LEFT)
							_page_press(p, (mEventPointer *)ev);
					}
					break;
				//離し
				case MEVENT_POINTER_ACT_RELEASE:
					if(p->fdrag && ev->pt.btt == MLK_BTT_LEFT)
						_page_ungrab(p);
					break;
			}
			break;

		//キー
		case MEVENT_KEYDOWN:
			if(!p->fdrag)
			{
				mWidgetEventAdd_notify_id(wg, MLK_WIDGET(g_app.mainwin), -1, 0,
					ev->key.key, ev->key.state);
			}
			break;

		case MEVENT_FOCUS:
			if(ev->focus.is_out)
				_page_ungrab(p);
			break;
	}

	return 1;
}

/* イメージ描画 */

static void _drawimage(ViewPage *p,mPixbuf *pixbuf,mImageBuf2 *img)
{
	uint8_t **ppsrc,*ps,*pd;
	int ix,iy,sw,sh,sx,sy,sxleft,srcleft;
	mPixCol col_bkgnd;
	mFuncPixbufSetBuf setpix;
	mPixbufClipBlt info;

	pd = mPixbufClip_getBltInfo(pixbuf, &info, 0, 0, p->wg.w, p->wg.h);
	if(!pd) return;
	
	sw = img->width;
	sh = img->height;
	sy = p->scry + info.sy;
	sxleft = p->scrx + info.sx;
	srcleft = sxleft * 3;
	ppsrc = img->ppbuf + sy;

	mPixbufGetFunc_setbuf(pixbuf, &setpix);

	col_bkgnd = MGUICOL_PIX(FACE);

	//

	for(iy = info.h; iy; iy--, sy++)
	{
		if(sy >= sh)
		{
			mPixbufFillBox(pixbuf, 0, info.h - iy, info.w, iy, col_bkgnd);
			break;
		}

		sx = sxleft;
		ps = *ppsrc + srcleft;
	
		for(ix = info.w; ix; ix--, sx++, pd += info.bytes)
		{
			if(sx >= sw)
				(setpix)(pd, col_bkgnd);
			else
			{
				(setpix)(pd, mRGBtoPix_sep(ps[0], ps[1], ps[2]));
				ps += 3;
			}
		}

		ppsrc++;
		pd += info.pitch_dst;
	}
}

/* 描画 */

static void _page_draw_handle(mWidget *wg,mPixbuf *pixbuf)
{
	mImageBuf2 *img;

	img = vs_get_frame_image(g_app.vs);

	if(!img)
		mPixbufFillBox(pixbuf, 0, 0, wg->w, wg->h, MGUICOL_PIX(FACE));
	else
		_drawimage((ViewPage *)wg, pixbuf, img);
}

/* サイズ変更時 */

static void _page_resize_handle(mWidget *wg)
{
	view_set_scroll_info(FALSE);
}

/* ViewPage 作成 */

static ViewPage *_create_page(mWidget *parent)
{
	ViewPage *p;

	p = (ViewPage *)mScrollViewPageNew(parent, sizeof(ViewPage));
	if(!p) return NULL;

	p->wg.draw = _page_draw_handle;
	p->wg.event = _page_event_handle;
	p->wg.resize = _page_resize_handle;
	
	p->wg.fevent |= MWIDGET_EVENT_POINTER | MWIDGET_EVENT_KEY;
	p->wg.fstate |= MWIDGET_STATE_TAKE_FOCUS | MWIDGET_STATE_ENABLE_KEY_REPEAT;

	return p;
}


//==========================
// View
//==========================


/* スクロール処理 */

static void _view_scroll(View *p,int pos,int flags,mlkbool vert)
{
	if(flags & MSCROLLBAR_ACTION_F_CHANGE_POS)
	{
		if(vert)
			g_app.viewpage->scry = pos;
		else
			g_app.viewpage->scrx = pos;
		
		mWidgetRedraw(MLK_WIDGET(g_app.viewpage));
	}
}

/* イベントハンドラ */

static int _event_handle(mWidget *wg,mEvent *ev)
{
	View *p = (View *)wg;

	switch(ev->type)
	{
		case MEVENT_NOTIFY:
			if(ev->notify.widget_from == MLK_WIDGET(p->sv.scrh))
			{
				//水平スクロール

				if(ev->notify.notify_type == MSCROLLBAR_N_ACTION)
					_view_scroll(p, ev->notify.param1, ev->notify.param2, FALSE);
			}
			else if(ev->notify.widget_from == MLK_WIDGET(p->sv.scrv))
			{
				//垂直スクロール

				if(ev->notify.notify_type == MSCROLLBAR_N_ACTION)
					_view_scroll(p, ev->notify.param1, ev->notify.param2, TRUE);
			}
			break;
	}

	return 1;
}

/** View 作成 */

void view_create(mWidget *parent)
{
	View *p;

	p = (View *)mScrollViewNew(parent, sizeof(View),
			MSCROLLVIEW_S_HORZVERT | MSCROLLVIEW_S_SCROLL_NOTIFY_SELF);
	if(!p) return;

	g_app.view = (mWidget *)p;

	p->wg.event = _event_handle;
	p->wg.flayout = MLF_EXPAND_WH;
	p->wg.notify_to = MWIDGET_NOTIFYTO_SELF;

	//page

	g_app.viewpage = _create_page(MLK_WIDGET(p));

	p->sv.page = (mScrollViewPage *)g_app.viewpage;
}

/** スクロール情報セット
 *
 * reset: 位置をクリアし、再構成 */

void view_set_scroll_info(mlkbool reset)
{
	View *p = (View *)g_app.view;
	mSize size;

	vs_get_video_size(g_app.vs, &size);

	mScrollBarSetStatus(p->sv.scrh, 0, size.w, p->sv.page->wg.w);
	mScrollBarSetStatus(p->sv.scrv, 0, size.h, p->sv.page->wg.h);

	if(reset)
	{
		mScrollBarSetPos(p->sv.scrh, 0);
		mScrollBarSetPos(p->sv.scrv, 0);
	}

	g_app.viewpage->scrx = mScrollBarGetPos(p->sv.scrh);
	g_app.viewpage->scry = mScrollBarGetPos(p->sv.scrv);

	if(reset)
		mScrollViewLayout(MLK_SCROLLVIEW(p));
}

/** ビューの更新 */

void view_update(void)
{
	mWidgetRedraw(MLK_WIDGET(g_app.viewpage));
}

