/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

/*****************************************
 * VapourSynth
 *****************************************/

#include <stdio.h>
#include <string.h>
#include <math.h>
#include <VSScript4.h>

#include "mlk_gui.h"
#include "mlk_widget.h"
#include "mlk_str.h"
#include "mlk_imagebuf.h"

#include "vs.h"


//---------------

struct _VSData
{
	const VSSCRIPTAPI *sf;
	const VSAPI *vsf;

	VSScript *script;
	VSNode *node_output;
	VSVideoInfo vinfo;

	mWidget *wg_notify;
	mImageBuf2 *img;	//フレームイメージ

	int err_init,	//1 で初期化に失敗
		wait_frame;	//フレーム取得待ちか
	double fps;
	mStr str_err;	//エラーメッセージ
};

#define _ERRMES_VSSCRIPT  ((void *)0)

//---------------


/* エラー文字列をセット
 *
 * mes: 0 = VSScript::getError() */

static void _set_err(VSData *p,const char *mes)
{
	if(mes == _ERRMES_VSSCRIPT)
		mes = p->sf->getError(p->script);

	if(!mes) return;

	mStrSetText(&p->str_err, mes);

	fprintf(stderr, "%s\n", mes);
}


//=============================


/* スクリプトの解放 */

static void _free_script(VSData *p)
{
	//出力ノード

	if(p->vsf && p->node_output)
		p->vsf->freeNode(p->node_output);

	//VSScript

	if(p->sf)
	{
		if(p->script)
			p->sf->freeScript(p->script);
	}

	//

	mImageBuf2_free(p->img);

	p->img = NULL;
	p->script = NULL;
	p->node_output = NULL;
	p->fps = 0;

	mMemset0(&p->vinfo, sizeof(VSVideoInfo));
}

/** 解放 */

void vs_free(VSData *p)
{
	if(!p) return;

	_free_script(p);

	mStrFree(&p->str_err);

	mFree(p);
}

/* 初期化 */

static void _initialize(VSData *p)
{
	//API 構造体のポインタ取得

	p->sf = getVSScriptAPI(VSSCRIPT_API_VERSION);

	if(!p->sf)
	{
		_set_err(p, "failed getVSScriptAPI()");
		return;
	}

	//VS API

	p->vsf = p->sf->getVSAPI(VAPOURSYNTH_API_VERSION);

	if(!p->vsf)
	{
		_set_err(p, "failed getVSAPI()");
		return;
	}

	//成功

	p->err_init = 0;
}

/** 初期化 */

VSData *vs_init(void)
{
	VSData *p;

	p = (VSData *)mMalloc0(sizeof(VSData));
	if(!p) return NULL;

	p->err_init = 1;

	_initialize(p);

	return p;
}

/** フレーム取得の通知ウィジェットをセット */

void vs_set_notify_widget(VSData *p,mWidget *wg)
{
	p->wg_notify = wg;
}

/** エラーメッセージがあるか */

mlkbool vs_has_errmes(VSData *p)
{
	return mStrIsnotEmpty(&p->str_err);
}

/** エラーメッセージを取得
 *
 * return: TRUE でエラーメッセージあり */

mlkbool vs_get_errmes(VSData *p,mStr *dst)
{
	if(mStrIsEmpty(&p->str_err))
		return FALSE;
	else
	{
		mStrCopy(dst, &p->str_err);

		return TRUE;
	}
}

/* RGB 変換フィルタを追加 */

static int _set_convert(VSData *p)
{
	const VSAPI *vsf = p->vsf;
	VSCore *core;
	VSPlugin *plugin;
	VSMap *map_arg,*map;
	const char *errmes;
	int err;

	//VSCore 取得

	core = p->sf->getCore(p->script);

	//プラグイン取得

	plugin = vsf->getPluginByID("com.vapoursynth.resize", core);

	if(!plugin)
	{
		_set_err(p, "failed get resize plugin");
		return 1;
	}

	//引数用マップ

	map_arg = vsf->createMap();

	vsf->mapSetNode(map_arg, "clip", p->node_output, maReplace);

	vsf->mapSetInt(map_arg, "format", pfRGB24, maReplace);

	if(p->vinfo.format.colorFamily == cfYUV)
		vsf->mapSetInt(map_arg, "matrix_in", 1, maReplace); //BT 709

	//フィルタに引数を渡して準備

	map = vsf->invoke(plugin, "Point", map_arg);
	
	vsf->freeMap(map_arg);

	errmes = vsf->mapGetError(map);
	if(errmes)
	{
		_set_err(p, errmes);
		vsf->freeMap(map);
		return 1;
	}

	//新しいノードを取得

	vsf->freeNode(p->node_output);

	p->node_output = vsf->mapGetNode(map, "clip", 0, &err);

	vsf->freeMap(map);

	if(!p->node_output)
	{
		_set_err(p, "failed get clip");
		return 1;
	}

	vsf->addNodeRef(p->node_output);

	return 0;
}

/** ファイル読み込み */

mlkbool vs_loadfile(VSData *p,const char *filename)
{
	const VSVideoInfo *vi;

	mStrEmpty(&p->str_err);

	_free_script(p);

	if(p->err_init) return FALSE;

	//VSScript 作成 (VSCore は自動作成)

	p->script = p->sf->createScript(NULL);

	if(!p->script)
	{
		_set_err(p, "failed createScript()");
		return FALSE;
	}

	//作業ディレクトリを、ファイルの位置に変更する

	p->sf->evalSetWorkingDir(p->script, 1);

	//スクリプトを読み込んで評価

	if(p->sf->evaluateFile(p->script, filename))
	{
		_set_err(p, _ERRMES_VSSCRIPT);
		return FALSE;
	}

	//出力ノードを取得

	p->node_output = p->sf->getOutputNode(p->script, 0);

	if(!p->node_output)
	{
		_set_err(p, "failed getOutputNode()");
		return FALSE;
	}

	//ビデオの情報取得

	vi = p->vsf->getVideoInfo(p->node_output);

	if(!vi)
	{
		_set_err(p, "failed getVideoInfo()");
		return FALSE;
	}

	p->vinfo = *vi;

	p->fps = (double)vi->fpsNum / vi->fpsDen;

	//RGB に変換

	if(vi->format.colorFamily != cfRGB
		|| vi->format.sampleType != stInteger
		|| vi->format.bitsPerSample != 8)
	{
		if(_set_convert(p))
			return FALSE;
	}

	//イメージの作成

	p->img = mImageBuf2_new(vi->width, vi->height, 24, -4);

	if(!p->img)
	{
		_set_err(p, "not enough memory");
		return FALSE;
	}

	mImageBuf2_clear0(p->img);

	return TRUE;
}

/* フレームコールバック */

static void _frame_callback(void *userdata,const VSFrame *f,int n,VSNode *node,const char *errmsg)
{
	VSData *p = (VSData *)userdata;
	uint8_t **ppdst,*pd;
	const uint8_t *psR,*psG,*psB;
	int ix,iy,w,pitchs;

	if(errmsg)
		_set_err(p, errmsg);

	//イメージにセット

	if(!f)
		mImageBuf2_clear0(p->img);
	else
	{
		ppdst = p->img->ppbuf;
		w = p->img->width;

		pitchs = p->vsf->getStride(f, 0) - w;
		psR = p->vsf->getReadPtr(f, 0);
		psG = p->vsf->getReadPtr(f, 1);
		psB = p->vsf->getReadPtr(f, 2);

		for(iy = p->img->height; iy; iy--)
		{
			pd = *(ppdst++);

			for(ix = w; ix; ix--, pd += 3)
			{
				pd[0] = *(psR++);
				pd[1] = *(psG++);
				pd[2] = *(psB++);
			}

			psR += pitchs;
			psG += pitchs;
			psB += pitchs;
		}

		p->vsf->freeFrame(f);
	}

	//

	p->wait_frame = 0;

	//イベント通知

	mWidgetEventAdd_base(p->wg_notify, VS_EVENT_GETFRAME);

	//イベント待ちループを抜ける

	mGuiThreadWakeup();
}

/** フレームの要求を行う */

mlkbool vs_getframe(VSData *p,int pos)
{
	if(!p->img || p->wait_frame)
		return FALSE;

	mStrEmpty(&p->str_err);

	p->wait_frame = 1;

	p->vsf->getFrameAsync(pos, p->node_output, _frame_callback, p);

	return TRUE;
}

/** フレームがまだ取得できていないか */

mlkbool vs_is_waitframe(VSData *p)
{
	return p->wait_frame;
}

/** ビデオの情報を文字列で取得 */

void vs_get_video_info(VSData *p,mStr *str)
{
	VSVideoInfo *vi = &p->vinfo;
	char fm[16];
	VSTime t;

	//fps

	if(vi->fpsNum == 24000 && vi->fpsDen == 1001)
		strcpy(fm, "23.976");
	else if(vi->fpsNum == 30000 && vi->fpsDen == 1001)
		strcpy(fm, "29.97");
	else
		snprintf(fm, 16, "%.3f", p->fps);

	//time

	vs_get_frame_time(p, vi->numFrames, &t);

	//

	mStrSetFormat(str, "%dx%d | fps: %s (%d/%d) | frames: %d (%d:%02d:%02d.%03d)",
		vi->width, vi->height,
		fm, (int)vi->fpsNum, (int)vi->fpsDen,
		vi->numFrames, t.h, t.m, t.s, t.ms);
}

/** ビデオサイズを取得 */

void vs_get_video_size(VSData *p,mSize *dst)
{
	dst->w = p->vinfo.width;
	dst->h = p->vinfo.height;
}

/** fps 取得 */

double vs_get_fps(VSData *p)
{
	return p->fps;
}

/** フレーム数を取得 */

int vs_get_frames(VSData *p)
{
	return p->vinfo.numFrames;
}

/** フレーム位置を範囲内に調整 */

int vs_adjust_frame_pos(VSData *p,int n)
{
	int frames = p->vinfo.numFrames;

	if(!frames || n < 0)
		n = 0;
	else if(n >= frames)
		n = frames - 1;

	return n;
}

/** フレームイメージ取得
 *
 * return: NULL でなし */

mImageBuf2 *vs_get_frame_image(VSData *p)
{
	return p->img;
}

/** フレーム位置から時間を取得 */

void vs_get_frame_time(VSData *p,int pos,VSTime *dst)
{
	double ds;
	int h,m;

	ds = pos / p->fps; //sec

	h = floor(ds / 3600);
	ds -= h * 3600.0;

	m = floor(ds / 60);
	ds -= m * 60.0;

	dst->h = h;
	dst->m = m;
	dst->s = floor(ds);
	dst->ms = floor(modf(ds, &ds) * 1000);
}


