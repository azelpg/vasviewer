/*$
vasviewer
Copyright (c) 2021-2022 Azel

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
$*/

typedef struct _VSData VSData;

#define VS_EVENT_GETFRAME 10000

typedef struct
{
	int h,m,s,ms;
}VSTime;


VSData *vs_init(void);
void vs_free(VSData *p);

void vs_set_notify_widget(VSData *p,mWidget *wg);

mlkbool vs_has_errmes(VSData *p);
mlkbool vs_get_errmes(VSData *p,mStr *dst);

mlkbool vs_loadfile(VSData *p,const char *filename);
mlkbool vs_getframe(VSData *p,int pos);
mlkbool vs_is_waitframe(VSData *p);
void vs_get_video_info(VSData *p,mStr *str);
void vs_get_video_size(VSData *p,mSize *dst);
double vs_get_fps(VSData *p);
int vs_get_frames(VSData *p);
int vs_adjust_frame_pos(VSData *p,int n);
mImageBuf2 *vs_get_frame_image(VSData *p);
void vs_get_frame_time(VSData *p,int pos,VSTime *dst);
